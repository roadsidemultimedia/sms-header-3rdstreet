/**
 * RS-BASIC.js
 * http://roadsidemultimedia.com/
 * Author: Curtis Grant
 */
    jQuery(function($) {
      $(window).ready(function() { 
          $('.rsnav').meanmenu();          
          var example = $('.rs-menu').superfish();     
          var navOffset = $('.rs-menu').offset();
              $(window).scroll(function(){
                if($(document).scrollTop() > navOffset.top)
                {
                    $(".fullheader").addClass("fixed");
                }
                else {
                    $(".fullheader").removeClass("fixed");
                }
              });
      });
      $( window ).resize(function() {
        if ($(window).width() < 1080) {
          $(".fullheader").removeClass('fixed');
        }
      });
    });