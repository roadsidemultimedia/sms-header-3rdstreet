<?php
/*
Plugin Name: SMS Header - 3rd Street
Plugin URI: http://www.roadsidemultimedia.com
Description: 3rd Street Navigation Header
Author: Barak Llewellyn and Curtis Grant
PageLines: true
Version: 1.0.3
Section: true
Class Name: SMSHeader3rdStreet
Filter: nav, full-width
Loading: active
Text Domain: dms-sms
Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-header-3rdstreet
Bitbucket Branch: master
*/
if( ! class_exists( 'PageLinesSectionFactory' ) )
  return;

class SMSHeader3rdStreet extends PageLinesSection {

  function section_styles(){
    
    wp_enqueue_style( 'superfish-css', $this->base_url.'/css/superfish.css');
    wp_enqueue_style( 'meanmenu-css', $this->base_url.'/css/meanmenu.css');
    wp_enqueue_script( 'rs-hoverIntent', $this->base_url.'/js/hoverIntent.js', array( 'jquery' ), pl_get_cache_key(), true );
    wp_enqueue_script( 'rs-basic-js', $this->base_url.'/js/rs.basic.js', array( 'jquery' ), pl_get_cache_key(), true );
    wp_enqueue_script( 'superfish-js', $this->base_url.'/js/superfish.js', array( 'jquery' ), pl_get_cache_key(), true );
    wp_enqueue_script( 'meanmenu-js', $this->base_url.'/js/jquery.meanmenu.js', array( 'jquery' ), pl_get_cache_key(), true );
    
  }
  function section_persistent(){
    //register_nav_menus( array( 'super_nav' => __( 'SuperNav Section', 'pagelines' ) ) );
  }

  function section_opts(){

    $opts = array(
      array(
        'type'    => 'text',
        'key'   => 'rs_nav_company_name',
        'label'   => __( 'Add the Company Name for Mobile View', 'pagelines' ),
        'default' => false,
        'scope' => 'global'
      ),
      array(
        'type'    => 'text',
        'key'   => 'rs_nav_company_gmap',
        'label'   => __( 'Add the Company Google Map Link full http', 'pagelines' ),
        'default' => false,
        'scope' => 'global',
      ),
      array(
        'type'    => 'text',
        'key'   => 'rs_nav_company_phone',
        'label'   => __( 'Add the Company Phone Number', 'pagelines' ),
        'default' => false,
        'scope' => 'global',
      ),
      array(
            'type'      => 'select',
            'key'     => 'rs_nav_cta_font',
            'label'     => 'Above Nav Font Style',
            'col' => 2,
            'opts'      => array(
              'font-serif'   => array('name' => 'Serif'),
              'font-serif-alt'  => array('name' => 'Serif (alt)'),
              'font-sans-serif'   => array('name' => 'Sans Serif'),            
              'font-condensed-serif'    => array('name' => 'Condensed Serif'),
              'font-condensed-sans-serif' => array('name' => 'Condensed Sans Serif'),
              'font-slab-serif'  => array('name' => 'Slab Serif'),
              'font-script'  => array('name' => 'Script'),
              'font-descriptive'  => array('name' => 'Decorative'),
            )
          ),
      array(
        'key' => 'rs_nav_cta_text',
        'type'  => 'text',
        'col' => 2,
        'label' => __( 'CTA Text', 'pagelines' ),
      ),
      array(
        'key' => 'rs_nav_cta_link',
        'type'  => 'text',
        'col' => 2,
        'label' => __( 'CTA Link', 'pagelines' ),
      ),
      array(
        'key' => 'rs_nav_cta_class',
        'type'  => 'text',
        'col' => 2,
        'label' => __( 'Extra CTA Class (ex. used for gforms popup)', 'pagelines' ),
      ),
      array(
        'type'  => 'multi',
        'key' => 'rs_nav_content',
        'title' => __( 'Logo', 'pagelines' ),
        'col' => 1,
        'opts'  => array(
          array(
            'type'  => 'image_upload',
            'key' => 'rs_nav_logo',
            'label' => __( 'Main Logo', 'pagelines' ),
            'has_alt' => true,
            'scope' => 'global',
          )
        )

      ),
      array(
        'type'  => 'multi',
        'key' => 'rs_nav_nav',
        'title' => 'Navigation',
        'col' => 2,
        'scope' => 'global',
        'opts'  => array(
          array(
            'key' => 'rs_nav_help',
            'type'  => 'help_important',
            'label' => __( 'Using Megamenus (multi column drop down)', 'pagelines' ),
            'help'  => __( 'Want a full width, multi column "mega menu"? Simply add a class of "megamenu" to the list items using the WP menu creation tool.', 'pagelines' )
          ),
          array(
            'key' => 'rs_nav_menu',
            'type'  => 'select_menu',
            'scope' => 'global',
            'label' => __( 'Select Menu', 'pagelines' ),
          ),
          array(
            'key' => 'rs_nav_search',
            'type'  => 'check',
            'label' => __( 'Hide Search?', 'pagelines' ),
          ),
          array(
            'key' => 'rs_nav_offset',
            'type'  => 'text_small',
            'place' => '100%',
            'label' => __( 'Dropdown offset from top of nav (optional)', 'pagelines' ),
            'help'  => __( 'Default is 100% aligned to bottom. Can be PX or %.', 'pagelines' )
          ),
          array(
            'type'      => 'text',
            'key'     => 'rs_nav_bg',
            'label'     => 'Navigation Background',
            'col' => 2,
          ),
        )
      )
    );

    return $opts;

  }

  /**
  * Section template.
  */
   function section_template( $location = false ) {

    // Global Variables (pl_setting)
    $menu = ( pl_setting('rs_nav_menu') ) ? pl_setting('rs_nav_menu') : false;
    $companyname = ( pl_setting('rs_nav_company_name') ) ? pl_setting('rs_nav_company_name') : "";
    $companymap = ( pl_setting('rs_nav_company_gmap') ) ? pl_setting('rs_nav_company_gmap') : "";
    $companyphone = ( pl_setting('rs_nav_company_phone') ) ? pl_setting('rs_nav_company_phone') : "";
    $companylogo = ( pl_setting('rs_nav_logo') ) ? pl_setting('rs_nav_logo') : "";

    // Local Variables ($this->opt)
    $class = ( $this->meta['draw'] == 'area' ) ? 'pl-content' : '';
    $hide_search = ( $this->opt('rs_nav_search') ) ? true : false;
    $offset = ( $this->opt('rs_nav_offset') ) ? sprintf( 'data-offset="%s"', $this->opt('rs_nav_offset') ) : false;
    $ctatext = ( $this->opt('rs_nav_cta_text') ) ? $this->opt('rs_nav_cta_text') : "Placeholder Text";
    $ctalink = ( $this->opt('rs_nav_cta_link') ) ? $this->opt('rs_nav_cta_link') : "#";
    $ctaclass = ( $this->opt('rs_nav_cta_class') ) ? $this->opt('rs_nav_cta_class') : "";
    $ctafontclass = ( $this->opt('rs_nav_cta_font') ) ? $this->opt('rs_nav_cta_font') : "";
    $rsnavbg = ( $this->opt('rs_nav_bg') ) ? $this->opt('rs_nav_bg') : "#FFF";


  ?>
<div class="fullheader">
    <div class="mmholder"></div>
    <div class="navholder basic shadow">
      <div class="rsnav">
        <div class="mobilemenu">
          <div class="rsphone">
            <a href="tel:<?php echo $companyphone; ?>"><i class="fa fa-mobile"></i><span><?php echo $companyphone; ?></span></a>
          </div>
          <div class="rsmap">
            <a href="<?php echo $companymap; ?>" target="_blank"><i class="fa fa-map-marker"></i><span>location</span></a>
          </div>
        </div>
        <?php

        $menu_args = array(
          'theme_location' => 'rsnav',
          'menu' => $menu,
          'menu_class'  => 'rs-menu bluefont',
          'depth'     => 0,
          'walker' => ''
        );
        echo wp_nav_menu( $menu_args );

      ?>

      </div>
    </div>
  </div>
  <div class="mspacer"></div>
<?php }

}


